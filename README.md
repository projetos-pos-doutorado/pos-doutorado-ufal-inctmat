[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](./LICENSE.md)

Projeto para estágio pós-doutoral na Universidade Federal de Alagoas - [UFAL](https://www.ufal.edu.br/), Brasil.

Projeto apresentado ao Instituto Nacional de Ciência e Tecnologia de Matemática -
[INCTMat](http://inctmat.impa.br/opencms/opencms/pt/index.html), atendendo à chamada para a participação na seleção de projetos para bolsas de estágio pós-doutoral, à ser executado no Programa de Pós-Graduação em Matemática da Universidade Federal de Alagoas  -
[PPGMat/UFAL](http://www.im.ufal.br/posgraduacao/posmat/index.php).
